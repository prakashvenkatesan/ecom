import { Component, ElementRef, HostListener } from '@angular/core';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss']
})
export class IndexComponent {
  sidenavStatus:boolean = false;
  navbtnicon: string = "fas fa-solid fa-bars";

  constructor(private elementRef: ElementRef){}

  toggleNav(){
    this.sidenavStatus = !this.sidenavStatus;
  }
  pageContent(){
    this.sidenavStatus = false;
  }
}
